package com.leon.leonlibrary.utils

import android.util.Log

object LeonUtils {
    fun showLog(msg: String?) {
        Log.i("TAG_LEON", "输出日志: $msg")
    }
}